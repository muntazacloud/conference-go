import requests
from events.keys import PIXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    header = {
        "Authorization": PIXELS_API_KEY,
    }
    # Create the URL for the request with the city and state
    url = f"https://api.pexels.com/v1/search?query={city} {state}&per_page=1"
    # Make the request
    response = requests.get(url, header=header)
    # Parse the JSON response
    data = response.json()
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response

    if "photos" in data and len(data["results"]["photos"]) > 0:

        photo_url = data["results"]["photos"][0]["url"]
    else:
        photo_url = None

    return {"photo_url": photo_url}


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    url = f"https://maps.googleapis.com/maps/api/geocode/json?address={city},+{state}&key={OPEN_WEATHER_API_KEY}"
    # Make the request
    response = requests.get(url)
    # Parse the JSON response
    data = response.json()
    if "results" in data and len(data["results"]) > 0:
        # Get the latitude and longitude from the response
        result = data["results"][0]
        location = result["geometry"]["location"]
        latitude = location["lat"]
        longitude = location["lng"]
        # Create the URL for the current weather API with the latitude
        #   and longitude
        url = f"https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={OPEN_WEATHER_API_KEY}"

        # Make the request
        response = requests.get(url)
    # Parse the JSON response
    data = response.json()
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    if "main" in data and "weather" in data:

        temperature = data["main"]["temp"]
        description = data["weather"][0]["description"]
        # Return the dictionary

        return {"weather": {"temp": temperature, "description": description}}
    # if weather info is unavailable , return None
    return {"weather": None}
