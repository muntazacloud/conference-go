from .models import Location, Conference, State
from django.http import JsonResponse
from .encoder import (
    ConferenceListEncoder,
    ConferenceDetailEncoder,
    LocationDetailEncoder,
    LocationListEncoder,
)

from django.views.decorators.http import require_http_methods
import json
from django.core.exceptions import ObjectDoesNotExist
from .acls import get_photo, get_weather_data

# Create your views here.


# Returns a dictionary with a single key "conferences" which is a list of conference names and URLS


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    new_response = []
    conference_list = Conference.objects.all()
    if request.method == "GET":
        for lists in conference_list:
            new_response.append(
                {
                    "name": lists.name,
                    "href": lists.get_api_url(),
                }
            )
            return JsonResponse(
                {"conference": new_response}, encoder=ConferenceListEncoder
            )
    elif request.method == "POST":
        content = json.loads(request.body)

        try:
            location_obj = Location.objects.get(id=content["location"])
            content["location"] = location_obj

        except Location.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"}, status=400)

        conference = Conference.objects.create(**content)
        return JsonResponse(
            {conference}, encoder=ConferenceListEncoder, safe=False
        )


def api_show_conference(request, id):
    try:
        conference = Conference.objects.get(id=id)

        # Use the city and state abbreviation of the Conference's Location
        # to call the get_weather_data ACL function and get back a dictionary
        # that contains the weather data

        weather_data = get_weather_data(
            conference.location.city, conference.location.state.abbreviation
        )
        return JsonResponse(
            {"conference": conference, "weather": weather_data},
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    except ObjectDoesNotExist:
        return JsonResponse({"message": "Error Raised"}, status=404)


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()

        return JsonResponse(
            {"locations": locations}, encoder=LocationDetailEncoder, safe=False
        )

    else:
        content = json.loads(request.body)
        # Cannot assign "'TX'": "Location.state" must be a "State" instance.
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"}, status=400
            )
            # ********** ERROR Handling for invalid states "
        # Use the city and state's abbreviation in the content dictionary
        # to call the get_photo ACL function

        photo_data = get_photo(content["city"], content["state"].abbreviation)
        if photo_data and "photo_url" in photo_data:
            content["photo_url"] = photo_data["photo_url"]
        location = Location.objects.create(**content)

        return JsonResponse(location, encoder=LocationListEncoder, safe=False)


# encoder => is serializing or converting  data structures into JSON format inorder to transmit data or response into the browser through http endpoint
# Because Json make it easy to parse and structure data
# class ConferenceDetailEncoder(ModelEncoder):


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    #  location not found error handling
    try:
        location = Location.objects.get(id=id)
    except ObjectDoesNotExist:
        return JsonResponse({"error": "location not found"}, status=400)
    if request.method == "GET":
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
