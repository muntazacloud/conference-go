from common.json import ModelEncoder
from .models import Conference, Location




# We will use this location in each encoder as encode
class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]

    # *******  Important note must add this encoder to each classes **********

    encoders = {
        "location": LocationListEncoder(),
    }
    ######## It just gets  any the extra data, from the state's abbreviation #######
    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]

    # *******  Important note must add this encoder to each classes **********

    encoders = {
        "location": LocationListEncoder(),
    }


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference

    properties = [
        "name",
        "description",
        "max_presentation",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
    ]

    # *******  Important note must add this encoder to each classes **********

    encoders = {
        "location": LocationListEncoder(),
    }
