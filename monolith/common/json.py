from json import JSONEncoder
from django.db.models import QuerySet
from datetime import datetime


# TypeError: Object of type QuerySet is not JSON serializable


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoder = {}

    def default(self, o):
        # if the incoming object stored in the o parameter is an instance of the class stored in the self.model property.
        if isinstance(o, self.model):
            dictionary = {}
            if hasattr(o, "get_api_url"):
                dictionary["href"] = o.get_api_url()
            for (
                property
            ) in (
                self.properties
            ):  # this properties is from the class encoder properties
                value = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                dictionary[property] = value
            dictionary.update(self.get_extra_data(o))
            return dictionary
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}
