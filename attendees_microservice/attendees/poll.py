import json
import requests
from .models import ConferenceVO


def get_conferences():  # This func will retrive data from remote API and then update or create ConferenceVO
    url = "http://monolith:8000/api/conferences/"  # url of the api endpoint  which expected to provide data in JSON format

    response = requests.get(
        url
    )  # this sends HTTP GET request to assigned url  and assign response
    content = json.loads(
        response.content
    )  # this parses the content of the response inot Json format
    for conference in content[
        "conferences"
    ]:  # this iterates over conferences within the content data structure
        ConferenceVO.objects.update_or_create(
            import_href=conference["href"],
            defaults={"name": conference["name"]},
        )  # This uses update_or_create to update or create  a ConferenceVO objects in the database, it takes two arguments if it matches the given argument it update or creates
